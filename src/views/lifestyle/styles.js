import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    safeArea: {
        marginTop: 25, 
        flex: 1, 
        backgroundColor: '#fff'
    },
    viewScroll: {
        flex: 1, 
        paddingBottom: 250
    }
})