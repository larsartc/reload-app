import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        marginHorizontal: 16
    },
    leftView: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center"
    },
    statusLabel: {
        marginLeft: 10,
        color: '#fff',
        fontSize: 15,
        fontWeight: "bold"
    },
    rightView: {
        flexDirection: "row",
        height: 50,
        alignItems: "center"
    },
    connectedView: {
        borderRadius: 20, 
        width: 75, 
        borderWidth: 1, 
        borderColor: '#fff', 
        height: 20, 
        alignItems: 'center', 
        justifyContent: "center"
    },
    connectedText: {
        fontSize: 10, 
        fontWeight: "bold", 
        color: '#fff'
    },
    image: {
        height: 60, 
        tintColor: "#fff"
    }
})