import React, { Component } from 'react'
import { Image, View, StyleSheet, Text } from 'react-native'
import { styles } from './styles'

const logo_fitbit = require('DesafioCricketz/src/assets/logo.png')
//const ic_arrow = require('DesafioCricketz/src/assets/ic_arrow.png')

export default class HeaderMap extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.leftView}>
                    <Text style={styles.statusLabel}>Traps and travels</Text>
                </View>
                <View style={styles.rightView}>
                    <View style={styles.connectedView}>
                        <Text style={styles.connectedText}>Connected</Text>
                    </View>
                    <Image source={logo_fitbit} resizeMode={"contain"} style={styles.image} />
                </View>
            </View>
        )
    }
}