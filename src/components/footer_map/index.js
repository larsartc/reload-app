import React, { Component } from 'react'
import { View, Image, Text } from 'react-native'

export default class FooterMap extends Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                <View style={{ backgroundColor: this.props.backgroundColor, width: 30, height: 30, borderRadius: 24, justifyContent: "center", alignItems: "center" }}>
                    <Image source={this.props.image} style={{ width: 15, height: 15 }} />
                </View>
                <Text style={{ fontWeight: "bold", fontSize: 14, color: '#fff' }}>{this.props.label}</Text>
                <Text style={{ fontWeight: "bold", fontSize: 12, color: '#fff' }}>{this.props.description}</Text>
            </View>
        )
    }
}
