import React, { Component } from 'react'
import { Text } from 'react-native'
import { styles } from './style'

export default class Labels extends Component {
    render() {
        return (
            <Text style={styles.label}>{this.props.description}</Text>
        )
    }
}