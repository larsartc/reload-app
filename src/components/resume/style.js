import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    viewChart: {
        elevation: 1, 
        borderWidth: 2, 
        backgroundColor: '#fff', 
        // marginHorizontal: 16,
        shadowRadius: 2, 
        shadowColor: '#000', 
        borderColor: "#fff", 
    },
    viewDetails: {
        flexDirection: "row", 
        justifyContent: "space-around"
    },
    viewLabels: {
        flexDirection: "row", 
        justifyContent: "space-between", 
        marginHorizontal: 16, 
        marginTop: 15
    },
    mapView: {
        borderRadius: 10,
        // width: '100%', 
        height: '50%', 
        backgroundColor: 'rgba(255, 0, 0, 0.3)',
        marginHorizontal: 16
    }
})