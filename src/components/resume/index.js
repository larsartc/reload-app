import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { styles } from './style'
import Divider from '../divider'
import { LineChart, Grid } from 'react-native-svg-charts'
import MapView, { Polyline } from 'react-native-maps'
import LinearGradient from 'react-native-linear-gradient';
import Details from '../details'
import Labels from '../labels'
import FooterMap from '../footer_map'
import HeaderMap from '../header_map'
import * as shape from 'd3-shape'


const ic_chart = require('DesafioCricketz/src/assets/ic_chart.png')
const ic_clock = require('DesafioCricketz/src/assets/clock.png')
const ic_fast = require('DesafioCricketz/src/assets/ic_fast.png')
const ic_noun_driving = require('DesafioCricketz/src/assets/noun-driving.png')
const ic_bike = require('DesafioCricketz/src/assets/invalid-name.png')

const coords = { latitude: -15.7983323, longitude: -47.8846251, latitudeDelta: 0.0922 * 10, longitudeDelta: 0.0421 / 10 }

export default class Resume extends Component {
    render() {
        return (
            <View style={styles.viewChart}>
                <View style={styles.viewDetails}>
                    <Details description={"10kms"} image={ic_chart} />
                    <Details description={"38 min"} image={ic_clock} />
                    <Details description={"4:50/KM"} image={ic_fast} />
                </View>
                <Divider hasMargin />
                <View style={styles.viewLabels}>
                    <Labels description={"Last run"} />
                    <Labels description={"12km - 48min"} />
                    <Labels description={"- 4:50/KM"} />
                </View>
                <LineChart
                    style={{ height: 200, marginHorizontal: 16 }}
                    data={[1, 10, 2, 20, 3, 10, 20, 30, 31]}
                    svg={{ stroke: 'rgb(14,194,184)' }}
                    contentInset={{ top: 20, bottom: 20 }}
                    curve={ shape.curveNatural }>
                </LineChart>
                <MapView
                    style={styles.mapView}
                    region={coords}>
                    <Polyline
                        strokeColor="#660f10"
                        strokeColors={['#fff']}
                        strokeWidth={2}
                        coordinates={[
                            { latitude: -15.7983323, longitude: -47.8846251 },
                            { latitude: -15.7375161, longitude: -47.8757884 },
                        ]}>
                    </Polyline>
                    <Polyline
                        strokeColor="#660f10"
                        strokeColors={['#fff']}
                        strokeWidth={2}
                        coordinates={[
                            { latitude: -15.7937841, longitude: -47.8857071 },
                            { latitude: -15.8248676, longitude: -47.9050141 },
                        ]}>
                    </Polyline>
                    <LinearGradient style={{ flex: 1 }} colors={['rgba(31,133,210,0.5984768907563025) 100%', 'rgba(246,246,246,0.5144432773109244) 33%', 'rgba(31,133,210,0.598476890756302) 100%)']}>
                        <View style={{ flex: 1, }}>
                            <HeaderMap/>
                        </View>
                        <View style={{ marginHorizontal: 20, flexDirection: "row", justifyContent: "space-between", alignItems: "flex-end", marginBottom: 22 }}>
                            <FooterMap label={"RUNNING"} description={"8.6 hours"} image={ic_noun_driving} backgroundColor={"#80deea"} />
                            <FooterMap label={"BICYCLE"} description={"1.1 hours"} image={ic_bike} backgroundColor={"#3f51b5"} />
                            <FooterMap label={"TRAVEL"} description={"39 minutes"} image={ic_noun_driving} backgroundColor={"#2196f3"} />
                        </View>
                    </LinearGradient>
                </MapView>
            </View>
        )
    }
}