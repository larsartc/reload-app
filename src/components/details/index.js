import React, { Component } from 'react'
import { View, Image, Text } from 'react-native'
import { styles } from './style'

const ic_chart = require('DesafioCricketz/src/assets/ic_chart.png')

export default class Details extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image source={this.props.image} style={styles.image} />
                <Text style={styles.text}>{this.props.description}</Text>
            </View>
        )
    }
}